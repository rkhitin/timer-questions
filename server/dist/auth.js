'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (app) {
  app.post('/login', function (req, res) {
    var _req$body = req.body,
        login = _req$body.login,
        password = _req$body.password;


    if (!(login === user.login && password === user.password)) return res.send(401);

    var token = _jsonwebtoken2.default.sign({}, jwtSecret);

    res.json({ token: token });
  });

  app.post('/is-auth', function (req, res) {
    var token = req.body.token;


    try {
      _jsonwebtoken2.default.verify(token, jwtSecret);
    } catch (err) {
      return res.json({ status: 'fail' });
    }

    res.json({ status: 'ok' });
  });
};

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var jwtSecret = 'someverysecretstring';

var user = {
  login: 'admin',
  password: '123456'
};