'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (io) {
  io.of('/questions').on('connection', function (socket) {
    getQuestions().then(function (questions) {
      io.of('/questions').emit('set questions', JSON.stringify(questions));
    });

    socket.on('insert question', function (questionText) {
      insertQuestion(questionText).then(function (question) {
        io.of('/questions').emit('add question', JSON.stringify(question));
      });
    });

    socket.on('remove question', function (_id) {
      removeQuestion(_id).then(function () {
        io.of('/questions').emit('delete question', _id);
      });
    });
  });
};

var _mongodb = require('mongodb');

var _mongodb2 = _interopRequireDefault(_mongodb);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var url = 'mongodb://localhost:27017/timerQuestions';

function insertQuestion(text) {
  return new Promise(function (resolve) {
    _mongodb.MongoClient.connect(url, function (err, db) {
      if (err) console.error(err);

      db.collection('questions').insertOne({
        text: text
      }, function (err, result) {
        if (err) return console.error(err);
        resolve(result.ops[0]);
      });
    });
  });
}

function removeQuestion(_id) {
  return new Promise(function (resolve) {
    _mongodb.MongoClient.connect(url, function (err, db) {
      if (err) console.error(err);

      db.collection('questions').deleteOne({
        _id: new _mongodb2.default.ObjectID(_id)
      }, function (err) {
        if (err) return console.error(err);
        resolve();
      });
    });
  });
}

function getQuestions() {
  return new Promise(function (resolve) {
    _mongodb.MongoClient.connect(url, function (err, db) {
      if (err) console.error(err);

      db.collection('questions').find({}).toArray(function (err, questions) {
        if (err) console.error(err);
        resolve(questions);
      });
    });
  });
}