'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _socket = require('socket.io');

var _socket2 = _interopRequireDefault(_socket);

var _http = require('http');

var _http2 = _interopRequireDefault(_http);

var _auth = require('./auth');

var _auth2 = _interopRequireDefault(_auth);

var _timer = require('./timer');

var _timer2 = _interopRequireDefault(_timer);

var _questions = require('./questions');

var _questions2 = _interopRequireDefault(_questions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _express2.default)();
var server = _http2.default.Server(app);
var io = (0, _socket2.default)(server);

app.use(_bodyParser2.default.json()); // to support JSON-encoded bodies
app.use(_bodyParser2.default.urlencoded({ // to support URL-encoded bodies
  extended: true
}));

(0, _timer2.default)(io);

(0, _questions2.default)(io);

(0, _auth2.default)(app);

var staticServe = _express2.default.static(__dirname + '/../static');

app.use("/", staticServe);
app.use("*", staticServe);

var port = app.get('env') === 'production' ? 80 : 3001;
server.listen(port, function () {
  console.log('Server listen port ' + port);
});