import mongodb, { MongoClient } from 'mongodb'

const url = 'mongodb://localhost:27017/timerQuestions'

function insertQuestion(text) {
  return new Promise(resolve => {
    MongoClient.connect(url, (err, db) => {
      if (err) console.error(err)

      db.collection('questions').insertOne({
        text
      }, (err, result) => {
        if (err) return console.error(err)
        resolve(result.ops[0])
      })
    })
  })
}

function removeQuestion(_id) {
  return new Promise(resolve => {
    MongoClient.connect(url, (err, db) => {
      if (err) console.error(err)

      db.collection('questions').deleteOne({
        _id: new mongodb.ObjectID(_id)
      }, err => {
        if (err) return console.error(err)
        resolve()
      })
    })
  })
}

function getQuestions() {
  return new Promise(resolve => {
    MongoClient.connect(url, (err, db) => {
      if (err) console.error(err)

      db.collection('questions').find({}).toArray((err, questions) => {
        if (err) console.error(err)
        resolve(questions)
      })
    })
  })
}

export default function(io) {
  io.of('/questions')
    .on('connection', socket => {
      getQuestions().then(questions => {
        io.of('/questions').emit('set questions', JSON.stringify(questions))
      })

      socket.on('insert question', questionText => {
        insertQuestion(questionText).then(question => {
          io.of('/questions').emit('add question', JSON.stringify(question))
        })
      })

      socket.on('remove question', _id => {
        removeQuestion(_id).then(() => {
          io.of('/questions').emit('delete question', _id)
        })
      })
    })
}