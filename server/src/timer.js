let timer = 10
let intervalId = null

export default function(io) {
  io.of('/timer')
    .on('connection', socket => {
      function startTimer() {
        if (timer <= 0) return

        io.of('/timer').emit('tick', JSON.stringify({ timer: --timer, isRun: true }))

        intervalId = setInterval(() => {
          if (timer === 0) {
            stopTimer()
          } else {
            io.of('/timer').emit('tick', JSON.stringify({ timer: --timer, isRun: true }))
          }
        }, 1000)
      }

      function stopTimer() {
        clearInterval(intervalId)

        intervalId = null

        io.of('/timer').emit('tick', JSON.stringify({ timer, isRun: false }))
      }

      function setTimer(newTimer) {
        timer = newTimer / 1

        io.of('/timer').emit('tick', JSON.stringify({ timer, isRun: !!intervalId }))
      }

      socket.emit('tick', JSON.stringify({ timer, isRun: !!intervalId }))

      //if (!!intervalId) startTimer()

      socket.on('start timer', startTimer)
      socket.on('stop timer', stopTimer)
      socket.on('set timer', setTimer)
    })
}