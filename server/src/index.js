import express from 'express'
import bodyParser from 'body-parser'
import socketIo from 'socket.io'
import http from 'http'

import auth from './auth'
import timer from './timer'
import questions from './questions'

const app = express()
const server = http.Server(app)
const io = socketIo(server)

app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

timer(io)

questions(io)

auth(app)

const staticServe = express.static(__dirname + '/../static')

app.use("/", staticServe)
app.use("*", staticServe)

const port = app.get('env') === 'production' ? 80 : 3001
server.listen(port, () => {
  console.log('Server listen port ' + port)
})