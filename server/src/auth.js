import jwt from 'jsonwebtoken'

const jwtSecret = 'someverysecretstring'

const user = {
  login: 'admin',
  password: '123456',
}

export default function(app) {
  app.post('/login', (req, res) => {
    const { login, password } = req.body

    if (!(login === user.login && password === user.password)) return res.send(401)

    const token = jwt.sign({}, jwtSecret)

    res.json({ token })
  })

  app.post('/is-auth', (req, res) => {
    const { token } = req.body

    try {
      jwt.verify(token, jwtSecret)
    } catch (err) {
      return res.json({ status: 'fail' })
    }

    res.json({ status: 'ok' })
  })
}