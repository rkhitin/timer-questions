#!/bin/bash

cd client

if [ $1 = "client" ]
then
    yarn run build
fi

cd ../server

rm -rf static/*

cd ..

cp -r client/build/* server/static/

cd server

yarn run build