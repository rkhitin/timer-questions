import { css } from 'glamor'

export default {
  home: css({
    display: 'flex',
    justifyContent: 'space-between',
    maxWidth: '400px',
    margin: '0 auto',
  }),
  link: css({
    height: '150px',
    width: '150px',
    backgroundColor: '#4c87bf',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white',
    textDecoration: 'none',
    fonsSize: '20px',

    ':hover': {
      backgroundColor: '#7299bf',
    }
  }),
}
