import React from 'react'
import { Link } from 'react-router-dom'

import styles from './styles'

const Home = () => (
  <div {...styles.home}>
    <Link {...styles.link} to="/timer">Таймер</Link>
    <Link {...styles.link} to="/questions">Вопросы</Link>
  </div>
)

Home.propTypes = {}

export default Home