import React from 'react'
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup'

import styles from './styles'

const Question = ({ questions }) => (
  <div {...styles.questions}>
    <h2 {...styles.title}>
      Вопросы:
    </h2>

    <CSSTransitionGroup
      transitionName="fade"
      transitionEnterTimeout={500}
      transitionLeaveTimeout={300}
    >
      {questions.map(q => (
        <div key={q._id} {...styles.question} >
          {q.text}
        </div>
      ))}
    </CSSTransitionGroup>
  </div>
)

Question.propTypes = {}

export default Question