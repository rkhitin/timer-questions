import React from 'react'
import io from 'socket.io-client'

import Questions from './Question'

class QuestionsContainer extends React.Component {
  state = {
    questions: [],
  }

  constructor(props) {
    super(props)

    this.socket = io.connect('/questions')
  }

  componentDidMount() {
    this.socket.on('set questions', message => {
      const questions = JSON.parse(message)
      this.setState({ questions })
    })

    this.socket.on('add question', message => {
      const question = JSON.parse(message)

      this.setState({
        questions: this.state.questions.concat([question])
      })
    })

    this.socket.on('delete question', _id => {
      this.setState({
        questions: this.state.questions.filter(q => q._id !== _id)
      })
    })
  }

  componentWillUnmount = () => {
    this.socket.off('set questions')
    this.socket.off('add question')
    this.socket.off('delete question')
  }

  render () {
    const questions = this.state.questions.slice(0, 4)

    return <Questions
      questions={questions}
    />
  }
}

export default QuestionsContainer