import { css } from 'glamor'

export default {
  questions: css({
    marginTop: '60px',
  }),
  title: css({
    fontSize: '30px',
    textAlign: 'center',
  }),
  question: css({
    minHeight: '100px',
    fontSize: '20px',
    padding: '0 15px',
    marginBottom: '15px',
    lineHeight: '1.2',
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    color: '#b0d5fb',

    '&:first-child': {
      color: 'white',
      fontSize: '30px',
      borderLeft: '3px solid white',
      borderRight: '3px solid white',
    },

    '&:not(:first-child):last-child::after': {
      position: 'absolute',
      bottom: 0,
      left: 0,
      height: '100%',
      width: '100%',
      content: '""',
      background: 'linear-gradient(to top, rgba(56,121,185, 1) 10%, rgba(255,255,255, 0) 90% )',
      pointerEvents: 'none',
    },
  }),
}
