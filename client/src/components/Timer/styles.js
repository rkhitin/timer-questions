import { css } from 'glamor'

export default {
  timerWrapper: css({
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  }),
  timer: css({
  }),
  title: css({
    fontSize: '30px',
    textAlign: 'center',
    fontWeight: '300',
    marginTop: 0,
  }),
  divider: css({
    fontSize: '80px',
    height: '64px',
    lineHeight: '50px',
    margin: '0 15px',
  }),
  timeBlocks: css({
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  }),
  timeBlock: css({
    height: '230px',
    width: '230px',
    backgroundColor: '#4c87bf',
    borderRadius: '20px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  }),
  time: css({
    fontSize: '170px',
    fontWeight: 300,
    flex: 1,
  }),
  label: css({
    fontSize: '16px',
    fontWeight: 200,
    paddingBottom: '15px',
  }),
}

