import React from 'react'
import io from 'socket.io-client'

import Timer from './Timer'

class TimerContainer extends React.Component {
  state = {
    timer: 0,
  }

  constructor(props) {
    super(props)

    this.socket = io.connect('/timer')
  }

  componentDidMount () {
    this.socket.on('tick', message => {
      const { timer } = JSON.parse(message)
      this.setState({ timer })
    })
  }

  componentWillUnmount = () => {
    this.socket.off('tick')
  }

  render () {
    const { timer } = this.state
    const seconds = this._addZero(timer % 60)
    const minutes = this._addZero(Math.floor(timer / 60))

    return <Timer seconds={seconds} minutes={minutes} />
  }

  _addZero(n) {
    if (n.toString().length < 2) return '0' + n

    return n
  }
}

export default TimerContainer