import React from 'react'

import styles from './styles'

const Timer = ({ minutes, seconds }) => (
  <div {...styles.timerWrapper}>
    <div {...styles.timer}>
      <h2 {...styles.title}>
        До окончания перерыва осталось:
      </h2>

      <div {...styles.timeBlocks}>

        <div {...styles.timeBlock}>
          <div {...styles.time}>{minutes}</div>
          <div {...styles.label}>мин</div>
        </div>

        <div {...styles.divider}>:</div>

        <div {...styles.timeBlock}>
          <div {...styles.time}>{seconds}</div>
          <div {...styles.label}>сек</div>
        </div>

      </div>
    </div>
  </div>
)

Timer.propTypes = {}

export default Timer