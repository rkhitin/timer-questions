import React from 'react'
import { Redirect } from 'react-router-dom'
import io from 'socket.io-client'

import isAuth from '../../lib/isAuth'

import QuestionsDashboard from './QuestionsDashboard'

class QuestionsDashboardContainer extends React.Component {
  state = {
    isAuth: true,
    questions: [],
    questionText: '',
  }

  constructor(props) {
    super(props)

    this.socket = io.connect('/questions')
  }

  componentDidMount() {
    isAuth().then(isAuth => this.setState({ isAuth }))

    this.socket.on('set questions', message => {
      const questions = JSON.parse(message)
      this.setState({ questions })
    })

    this.socket.on('add question', message => {
      const question = JSON.parse(message)

      this.setState({
        questions: this.state.questions.concat([question])
      })
    })

    this.socket.on('delete question', _id => {
      this.setState({
        questions: this.state.questions.filter(q => q._id !== _id)
      })
    })
  }

  componentWillUnmount = () => {
    this.socket.off('set questions')
    this.socket.off('add question')
    this.socket.off('delete question')
  }

  insertQuestion = () => {
    this.socket.emit('insert question', this.state.questionText)
    this.setState({ questionText: '' })
  }

  removeQuestion = (_id) => {
    this.socket.emit('remove question', _id)
  }

  changeQuestionText = (e) => {
    this.setState({ questionText: e.target.value })
  }

  render () {
    const { isAuth, questions, questionText } = this.state
    const { changeQuestionText, insertQuestion, removeQuestion } = this

    if (! isAuth) return <Redirect to="/login" />

    return <QuestionsDashboard
      questions={questions}
      questionText={questionText}
      changeQuestionText={changeQuestionText}
      insertQuestion={insertQuestion}
      removeQuestion={removeQuestion}
    />
  }
}

export default QuestionsDashboardContainer