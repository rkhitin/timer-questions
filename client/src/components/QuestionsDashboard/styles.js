import { css } from 'glamor'

export default {
  controls: css({
    display: 'flex',
    flexDirection: 'column',
    marginBottom: '30px',
  }),
  question: css({
    marginBottom: '15px',
  }),
  remove: css({
    color: '#84140d',
    cursor: 'pointer',

    '&:hover': {
      color: '#a65b58',
    },
  }),
}
