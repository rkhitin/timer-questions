import React from 'react'

import styles from './styles'

const QuestionsDashboard = ({ removeQuestion, questionText, changeQuestionText, insertQuestion, questions }) => (
  <div>
    <div {...styles.controls}>
      <textarea rows="10" cols="40" value={questionText} onChange={changeQuestionText} />
      <button onClick={insertQuestion}>Добавить</button>
    </div>

    <div>
      {questions.map(q => (
        <div key={q._id} {...styles.question} >
          {q.text}
          <span {...styles.remove} onClick={() => removeQuestion(q._id)}>✕</span>
        </div>
      ))}
    </div>
  </div>
)

QuestionsDashboard.propTypes = {}

export default QuestionsDashboard