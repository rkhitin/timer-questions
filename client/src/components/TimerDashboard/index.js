import React from 'react'
import { Redirect } from 'react-router-dom'
import io from 'socket.io-client'
import moment from 'moment'

import isAuth from '../../lib/isAuth'

import TimerDashboard from './TimerDashboard'

class TimerDashboardContainer extends React.Component {
  state = {
    isAuth: true,
    timer: 0,
    isRun: false,
    newTimer: 0,
  }

  constructor(props) {
    super(props)

    this.socket = io.connect('/timer')
  }

  componentDidMount = () => {
    isAuth().then(isAuth => this.setState({ isAuth }))

    this.socket.on('tick', message => {
      const { timer, isRun } = JSON.parse(message)
      this.setState({ timer, isRun })
    })
  }

  componentWillUnmount = () => {
    this.socket.off('tick')
  }

  changeNewTimer = (m) => {
    const newTimer = m.minutes() * 60 + m.seconds()

    this.setState({ newTimer })
  }

  setTimer = () => {
    this.socket.emit('set timer', this.state.newTimer)
  }

  toggleTimer = () => {
    if (this.state.isRun) return this.socket.emit('stop timer')

    this.socket.emit('start timer')
  }

  render () {
    const { isAuth, timer, isRun, newTimer } = this.state
    const { toggleTimer, changeNewTimer, setTimer } = this

    if (! isAuth) return <Redirect to="/login" />

    const seconds = this._addZero(timer % 60)
    const minutes = this._addZero(Math.floor(timer / 60))

    return <TimerDashboard
      seconds={seconds}
      minutes={minutes}
      isRun={isRun}
      toggleTimer={toggleTimer}
      newTimer={this._convertSecondsToMoment(newTimer)}
      changeNewTimer={changeNewTimer}
      setTimer={setTimer}
    />
  }

  _convertSecondsToMoment = (seconds) => {
    const duration = moment.duration(seconds, 'seconds')

    return moment().set({ hours: 0, minutes: duration.minutes(), seconds: duration.seconds() })
  }

  _addZero(n) {
    if (n.toString().length < 2) return '0' + n

    return n
  }
}

export default TimerDashboardContainer