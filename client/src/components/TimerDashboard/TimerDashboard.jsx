import React from 'react'
import TimePicker from 'rc-time-picker'

import 'rc-time-picker/assets/index.css'
import styles from './styles'

const TimerDashboard = ({ minutes, seconds, isRun, toggleTimer, newTimer, changeNewTimer, setTimer }) => (
  <div {...styles.dashboard}>

    <div {...styles.timerBlock}>
      <div {...styles.timer}>
        <div>
          {minutes}
        </div>
        <div>
          :
        </div>
        <div>
          {seconds}
        </div>
      </div>

      <button onClick={toggleTimer}>{ isRun ? 'Остановить' : 'Запустить' }</button>
    </div>

    <div {...styles.timerBlock}>
      <TimePicker value={newTimer} onChange={changeNewTimer} showHour={false} />
      <button onClick={setTimer}>Установить таймер</button>
    </div>

  </div>
)

TimerDashboard.propTypes = {}

export default TimerDashboard