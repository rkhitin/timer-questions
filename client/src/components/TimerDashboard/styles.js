import { css } from 'glamor'

export default {
  dashboard: css({
    paddingTop: '120px',
    width: '300px',
    margin: '0 auto',
  }),
  timerBlock: css({
    display: 'flex',
    justifyContent: 'space-around',
    marginBottom: '40px',
    alignItems: 'center',
  }),
  timer: css({
    display: 'flex',
    fontSize: '30px',
  }),
}
