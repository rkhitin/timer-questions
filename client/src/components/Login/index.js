import React from 'react'
import Login from './Login'

import isAuth from '../../lib/isAuth'

class LoginContainer extends React.Component {
  state = {
    login: '',
    password: '',
    error: '',
    isAuth: false,
  }

  componentDidMount = () => isAuth().then(isAuth => this.setState({ isAuth }))

  changeLogin = e => this.setState({ login: e.target.value })

  changePassword = e => this.setState({ password: e.target.value })

  login = () => {
    const { login, password } = this.state

    fetch('/login', {
      method: 'post',
      headers: {
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
      },
      body: `login=${login}&password=${password}`
    }).then(response => {
      if (response.status === 401) this.setState({ error: 'Не правильные логин и пароль' })

      return response.json()
    }).then(json => {
      const { token } = json

      if (!token) this.setState({ error: 'Что-то пошло не так' })

      window.localStorage.accessToken = token

      this.setState({ isAuth: true })

      window.location = '/login'
    })
  }

  render () {
    const { login, password, isAuth, error } = this.state
    const actions = {
      changeLogin: this.changeLogin,
      changePassword: this.changePassword,
      login: this.login,
    }

    return <Login
      error={error}
      isAuth={isAuth}
      login={login}
      password={password}
      actions={actions}
    />
  }
}

export default LoginContainer