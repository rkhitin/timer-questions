import React from 'react'

import styles from './styles'

const Login = ({ login, password, error, isAuth, actions }) => (
  <div {...styles.login}>
    {error ?
      <div>
        {error}
      </div>
    :''}
    {isAuth ?
      <div>
        Вы авторизованы
      </div>
    :
      <div {...styles.form}>
        <input {...styles.input} type="text" value={login} onChange={actions.changeLogin} />
        <input {...styles.input} type="text" value={password} onChange={actions.changePassword} />
        <button onClick={actions.login}>Login</button>
      </div>
    }
  </div>
)

Login.propTypes = {}

export default Login