import { css } from 'glamor'

export default {
  login: css({
    paddingTop: '100px',
  }),
  form: css({
    margin: '0 auto',
    maxWidth: '400px',
    display: 'flex',
    flexDirection: 'column',

    '&>*': {
      marginBottom: '15px',
    }
  }),
  input: css({
    height: '30px',
    fontSize: '18px',
  })
}
