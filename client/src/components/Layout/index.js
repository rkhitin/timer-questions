import React from 'react'

import isAuth from '../../lib/isAuth'

import Layout from './Layout'

class LayoutContainer extends React.Component {
  state = {
    isAuth: false,
  }

  componentDidMount() {
    try {
      isAuth().then(isAuth => this.setState({ isAuth }))
    } catch(err) {

    }
  }

  render () {
    const { isAuth } = this.state
    const { children } = this.props

    return <Layout isAuth={isAuth}>{children}</Layout>
  }
}

export default LayoutContainer