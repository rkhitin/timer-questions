import React from 'react'
import { Link } from 'react-router-dom'

import './base.css'
import styles from './styles'

const Layout = ({ isAuth, children }) => {
  const layoutStyles = isAuth
    ? { ...styles.layout, ...styles.layoutWithMenu }
    : { ...styles.layout }

  return (
    <div {...layoutStyles}>
      {isAuth ?
        <div {...styles.menu}>
          <Link to="/">Home</Link>
          <Link to="/login">Вход</Link>
          <Link to="/timer">Таймер</Link>
          <Link to="/questions">Вопросы</Link>
          <Link to="/admin/questions">Админка вопросов</Link>
          <Link to="/admin/timer">Админка таймера</Link>
        </div>
        :''}

      <div {...styles.content}>
        {children}
      </div>
    </div>
  )
}

Layout.propTypes = {}

export default Layout