import { css } from 'glamor'

export default {
  layout: css({
    maxWidth: '800px',
    margin: '0 auto',
    padding: '0 30px',
    height: '100%',

    '@media(min-width: 800px)': {
      padding: '0',
    }
  }),
  layoutWithMenu: css({
    paddingTop: '40px',
  }),
  menu: css({
    position: 'absolute',
    left: 0,
    top: 0,
    padding: '10px',
    backgroundColor: '#6890b9',

    '&>*': {
      marginRight: '15px',
    },

    '&>a': {
      color: 'white',
    },
  }),
  content: css({
    height: '100%',
  }),
}
