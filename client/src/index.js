import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import 'whatwg-fetch'

import Layout from './components/Layout'
import Home from './components/Home'
import Questions from './components/Questions'
import Timer from './components/Timer'
import TimerDashboard from './components/TimerDashboard'
import QuestionsDashboard from './components/QuestionsDashboard'
import Login from './components/Login'

const App = () => (
  <Router>
    <Layout>
      <Route exact path="/" component={Home}/>
      <Route path="/timer" component={Timer}/>
      <Route path="/questions" component={Questions}/>
      <Route path="/login" component={Login}/>
      <Route onEnter path="/admin/questions" component={QuestionsDashboard}/>
      <Route path="/admin/timer" component={TimerDashboard}/>
    </Layout>
  </Router>
)

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
