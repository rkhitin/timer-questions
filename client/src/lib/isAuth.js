export default function() {
  return new Promise(resolve => {
    if (!window.localStorage.accessToken) return resolve(false)

    fetch('/is-auth', {
      method: 'post',
      headers: {
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
      },
      body: `token=${window.localStorage.accessToken}`
    }).then(response => {
      return response.json()
    }).then(json => {
      resolve(json.status === 'ok')
    }).catch(err => {
      console.dir(err)
      resolve(false)
    })
  })
}